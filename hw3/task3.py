import matplotlib.pyplot as plt
from scipy.misc import derivative
import numpy as np

def function(x):
    # Edit return value below to get plot of another function
    return np.sin(x)

def deriv(x):
    return derivative(function, x, dx=0.1)

y = np.arange(-2*np.pi, 2*np.pi, 0.1)

plt.plot(y, function(y), label="f(x)")
plt.plot(y, deriv(y), label="f'(x)")

plt.legend(loc="upper right")
plt.grid(True)

plt.show()
