from argparse import ArgumentParser
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator
import numpy as np
from enum import Enum

func_str = "f(x, y) = x^2 - y^2"

def f(x, y):
    return x**2-y**2

class Command(Enum):
    surface = "surface"
    field = "field"

    def __str__(self):
        return self.value


def build_surface():
    x = np.linspace(-10, 10, 20)
    y = np.linspace(-10, 10, 20)

    X, Y = np.meshgrid(x, y)
    Z = f(X, Y)

    ax = plt.axes(projection="3d")
    ax.contour3D(X, Y, Z, 50, cmap="binary")

    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_zlabel("z")
    plt.title(func_str)
    plt.show()

def build_field():
    x = np.arange(-2, 2, 0.2)
    y = np.arange(-2, 2, 0.2)

    X, Y = np.meshgrid(x, y)

    dy = f(X,Y)
    dx = np.ones(dy.shape)

    plt.quiver(X, Y, dx, dy, color="red")
    plt.title("Field of directions for {}".format(func_str))
    plt.show()

if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("command", type=Command, choices=list(Command))

    args = parser.parse_args()
    if args.command == Command.surface:
        build_surface()
    elif args.command == Command.field:
        build_field()
    else:
        parser.print_help()
