import collections
import heapq

def network_delay(times, N, X):
        graph = collections.defaultdict(list)

        for start, finish, time in times:
            if start == finish:
                return -1
            graph[start].append((finish, time))
        if X not in graph:
            return -1

        queue = [(0,X)]
        visited = {}
        while queue:
            step, source_node = heapq.heappop(queue)
            if source_node in visited:
                continue
            visited[source_node] = step
            for target, distance in graph[source_node]:
                if target not in visited:
                    heapq.heappush(queue, (step+distance, target))

        return max(visited.values()) if len(visited) == N else -1

def main():
    print("Enter triplets (u, v, w) on each line, comma separated (e.g. `1,2,2`)")
    times = []

    while node := input():
        times.append(list(map(int, node.split(','))))

    N = int(input("Enter node amount (N):\n"))

    if N < 1 or N > 100:
        print("N should be in interval [1, 100]")
        exit(1)

    X = int(input("Enter node number (X):\n"))

    if X < 1 or X > N:
        print("X should be in interval [1, N]")
        exit(1)

    for time in times:
        if (time[0] < 1 or time[0] > N) or (time[1] < 1 or time[1] > N):
            print("u and v should be in interval [1, N]")
            exit(1)
        if time[2] < 1 or time[2] > 100:
            print("w should be in interval [1, 100]")
            exit(1)

    print(network_delay(times, N, X))


if __name__ == "__main__":
    main()
