class MedianFinder:
    _max_len = 5 * (10 ** 5)
    _left_border = (-10) ** 5
    _right_border = 10 ** 5

    def __init__(self) -> None:
        self._data = []

    def add_num(self, num: float) -> None:
        if len(self._data) >= self._max_len:
            raise RuntimeError("data list size must be less then {}".format(self._max_len))
        if num < self._left_border or num > self._right_border:
            raise RuntimeError("elements must be in segment [{}, {}], got: {}".format(
                self._left_border, self._right_border, num
            ))

        num = float(num)
        self._data.append(num)

    def find_median(self) -> float:
        self._data = sorted(self._data)
        n = len(self._data)
        if n == 0:
            return 0

        if n % 2 == 1:
            return round(self._data[n // 2], 5)
        else:
            i = n // 2
            return round((self._data[i - 1] + self._data[i]) / 2, 5)
