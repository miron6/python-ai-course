from median_finder import MedianFinder

median_finder = MedianFinder()
assert median_finder.find_median() == .0

median_finder.add_num(1)
median_finder.add_num(2)
assert median_finder.find_median() == 1.5

median_finder.add_num(3)
assert median_finder.find_median() == 2.

median_finder.add_num(1023)
median_finder.add_num(1313.43223)
median_finder.add_num(-1231.1)
median_finder.add_num(42349.2222)
median_finder.add_num(0)
median_finder.add_num(-666.666)
median_finder.add_num(777.777)
assert median_finder.find_median() == 2.5

median_finder = MedianFinder()
median_finder.add_num(1)
median_finder.add_num(1.8)
median_finder.add_num(1.25)
median_finder.add_num(1.333)
assert median_finder.find_median() == 1.2915

# Errors examples:
# median_finder = MedianFinder()
# for i in range(0, 5 * 10 ** 5 + 1):
#     median_finder.add_num(i)
# median_finder.add_num(10**5 + 0.0001)
