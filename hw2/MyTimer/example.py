import time
from my_timer import MyTimer

with MyTimer("s") as t:
    time.sleep(12)

assert t.elapsed_time() == 12

with MyTimer("m") as t:
    time.sleep(62)

assert t.elapsed_time() == 1

with MyTimer("h") as t:
    time.sleep(2)

assert t.elapsed_time() == 0
