import time
from enum import Enum

class Units(Enum):
    Seconds = "s"
    Minutes = "m"
    Hours = "h"

class TimerError(Exception):
    """A custom exception used to report errors in use of MyTimer class"""

class MyTimer:
    def __init__(self, units: str) -> None:
        self._units = Units(units)
        self._start_time = None
        self._finish_time = None

    def __enter__(self) -> 'MyTimer':
        if self._start_time is not None:
            raise TimerError("timer already in work")
        self._start_time = time.perf_counter()
        return self

    def __exit__(self, type, value, traceback) -> None:
        self._finish_time = time.perf_counter()

    def elapsed_time(self) -> int:
        spent_time = self._finish_time - self._start_time

        self._start_time = None
        self._finish_time = None

        if self._units == Units.Seconds:
            return int(spent_time)
        elif self._units == Units.Minutes:
            return int(spent_time // 60)
        elif self._units == Units.Hours:
            return int(spent_time // 3600)
