def check_sequence(data):
    brackets_assoc = {
        "}": "{",
        ")": "(",
        "]": "["
    }
    open_brackets = ["{", "(", "["]
    close_brackets = ["}", ")", "]"]
    
    brackets = []
    for c in data:
        if c in open_brackets:
            brackets.append(c)
        elif c in close_brackets:
            if len(brackets) == 0:
                return False
            if brackets.pop() != brackets_assoc.get(c):
                return False
    return len(brackets) == 0

def main():
    print("Input your sequence:")
    input_data = input()
    print(check_sequence(input_data))

if __name__ == "__main__":
    main()
