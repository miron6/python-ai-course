def find_triplets(arr, a, b, c):
    triplets = []
    triplets_amount = 0
    
    if len(arr) < 3:
        return triplets, triplets_amount
    
    for i in range(len(arr) - 2):
        for j in range(i + 1, len(arr) - 1):
            for k in range(j + 1, len(arr)):
                if (
                    abs(arr[i] - arr[j]) <= a and
                    abs(arr[j] - arr[k]) <= b and
                    abs(arr[i] - arr[k]) <= c
                ):
                    triplets.append((arr[i], arr[j], arr[k]))
                    triplets_amount += 1
    
    return triplets, triplets_amount

def main():
    print("Enter int array (comma separated, e.g.: 1,2,3):")
    arr = list(map(int, input().split(",")))
    print("Enter a:")
    a = int(input())
    print("Enter b:")
    b = int(input())
    print("Enter c:")
    c = int(input())

    triplets, triplets_amount = find_triplets(arr, a, b, c)
    print("Amount: {}".format(triplets_amount))
    print("No triplets.") if triplets_amount == 0 else print("There are {} triplets: {}".format(triplets_amount, triplets))

if __name__ == "__main__":
    main()
