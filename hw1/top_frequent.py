from collections import defaultdict

def top_k_frequent(arr, k):
    counter = defaultdict(int)

    for elem in arr:
        counter[elem] += 1

    return sorted(counter.keys(), key=counter.get, reverse=True)[:k]


def main():
    print("Enter int array (comma separated, e.g.: 1,2,3):")
    arr = list(map(int, input().split(",")))
    print("Enter k:")
    k = int(input())

    print(top_k_frequent(arr, k))

if __name__ == "__main__":
    main()
